﻿--insert into texts1(textt, title) values('слон кошка собака собака пришла капуста собака', 'example2')

--select to_tsvector('russian', textt) from texts

--select to_tsvector('russian', ff::text) from (SELECT regexp_split_to_table(textt, '[.!?]') from texts) as ff

/*create or replace function reorder_tsvector1(vector tsvector)
returns text language sql as $$
    select array_to_string(st, ',')
    from(
    select array_agg(ar[1] order by pos::int) st
    from (
        select string_to_array(elem, ':') ar
        from unnest(string_to_array(vector::text, ' ')) elem
        ) s,
    unnest(string_to_array(ar[2], ',')) pos) f
$$;*/
select reorder_tsvector1(to_tsvector('russian', textt)) from texts

--SELECT regexp_split_to_table(textt, '[.!?]') from texts

--select reorder_tsvector1(to_tsvector('russian', ff::text)) from (SELECT regexp_split_to_table(textt, '[.!?]') from texts) ff

/*CREATE or replace FUNCTION reorder_tsvector_trig() RETURNS trigger AS $$
    BEGIN
        insert into words1(word) select array_agg(ar[1])
    from (
        select string_to_array(elem, ':') ar
        from unnest(string_to_array((select to_tsvector('russian', NEW.textt) from texts1)::text, ' ')) elem
        ) s;
    RETURN NEW;
    END;
$$ LANGUAGE plpgsql;*/

/*create or replace function no_matches1(vector tsvector)
returns text language sql as $$
    select array_to_string(st, ',')
    from (
    select array_agg(ar[1]) st
    from (
        select string_to_array(elem, ':') ar
        from unnest(string_to_array(vector::text, ' ')) elem
        ) s) f
$$;*/

--select no_matches1(to_tsvector('russian', textt)) from texts

/*CREATE TRIGGER add_word
after insert ON texts1
FOR EACH ROW EXECUTE PROCEDURE reorder_tsvector_trig();*/

--select reorder_tsvector1(to_tsvector('russian', textt)) from texts

/*insert into words1(word) select array_agg(ar[1])
    from (
        select string_to_array(elem, ':') ar
        from unnest(string_to_array((select to_tsvector('russian', textt) from texts where id_t = 4)::text, ' ')) elem
        ) s;*/


--select word_count from words order by word_count desc